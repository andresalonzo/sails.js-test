$(document).ready(function(){

               // Validate
               // http://bassistance.de/jquery-plugins/jquery-plugin-validation/
               // http://docs.jquery.com/Plugins/Validation/
               // http://docs.jquery.com/Plugins/Validation/validate#toptions

                   $('#sign-up-form').validate({
                   rules: {
                     name: {
                       required: true
                     },
                     lastname: {
                       required: true
                     },
                     email: {
                       required: true,
                       email: true
                     },
                     password: {
                       minlength: 6,
                       required: true
                     },
                     confirmation: {
                       minlength: 6,
                       equalTo: "#password"
                     }
                   },
                   messages: {
                     name: "Por favor ingresa tu nombre",
                     lastname: "Por favor ingresa tu apellido",
                     email: "Ingresa un email valido",
                     password: {
                       required: "Ingresa una contraseña",
                       minlength: "Tu contraseña debe tener un minimo de 6 caracteres"
                     },
                     confirmation: {
                       required: "Ingresa la confirmación de la contraseña",
                       minlength: "Tu contraseña debe tener un minimo de 6 caracteres",
                       equalTo: "La contraseña y la confirmación deben ser iguales"
                     }
                   },
                   success: function(element) {
                     element.text('').addClass('valid')
                   }
                 });
           });
